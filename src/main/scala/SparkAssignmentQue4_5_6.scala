import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object SparkAssignmentQue4_5_6 {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.ERROR);
    val spark = SparkSession
      .builder
      .appName("IndianTrade")
      .master("local[*]")
      .getOrCreate()

    val export = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load("/Users/Speigan/Desktop/RKG/india-trade-data/2018-2010_export.csv")
    val import_data = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load("/Users/Speigan/Desktop/RKG/india-trade-data/2018-2010_import.csv")
    val year = 2018
    val country = "U S A"

/*
  4. For any particular year and country find whether India was net importer or exporter
 */
    val select_country_year_export = export.select("value").where(s"country = '$country' and year = $year").agg(sum("value"))
    val select_country_year_import = import_data.select("value").where(s"country = '$country' and year = $year").agg(sum("value"))
    if (select_country_year_export.first().getDouble(0) > select_country_year_import.first().getDouble(0))
      println(s"India was a net exporter for $country during $year")
    else
      println(s"India was a net importer for $country during $year")


/*
 5. For a year find if India was a net importer or exporter
 */
    val select_year_export = export.select("value").where(s"year = $year").agg(sum("value"))
    val select_year_import = import_data.select("value").where(s"year = $year").agg(sum("value"))
    if(select_year_export.first().getDouble(0) > select_year_import.first().getDouble(0))
      println(s"India was a net exporter during $year")
    else
      println(s"India was a net importer during $year")

/*
6. For a given commodity find if India has been a net importer or exporter
 */
    val commodity = "MINERAL FUELS, MINERAL OILS AND PRODUCTS OF THEIR DISTILLATION; BITUMINOUS SUBSTANCES; MINERAL WAXES."
    val select_commodity_export = export.select("value").where(s"commodity = '$commodity'").agg(sum("value"))
    val select_commodity_import = import_data.select("value").where(s"commodity = '$commodity'").agg(sum("value"))
    if(select_commodity_export.first().getDouble(0) > select_commodity_import.first().getDouble(0))
      println(s"India was a net exporter for $commodity")
    else
      println(s"India was a net importer for $commodity")


  }
}