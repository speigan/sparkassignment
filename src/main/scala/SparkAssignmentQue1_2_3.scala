import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.sql
import org.apache.spark.sql.SparkSession

object SparkAssignmentQue1_2_3  {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val spark = SparkSession.builder.appName("SparkAssignment").master("local[*]").getOrCreate()

    val dataExport = spark.read.format("csv").option("header","true").option("inferSchema","true").load("/Users/Speigan/Desktop/RKG/india-trade-data/2018-2010_export.csv").toDF()
    val dataImport = spark.read.format("csv").option("header","true").option("inferSchema","true").load("/Users/Speigan/Desktop/RKG/india-trade-data/2018-2010_import.csv")


    /*
    1. What did India export the most in any given year?
     */
    val ans1 = dataExport.filter("year == 2018")
      .groupBy("Commodity")
      .sum("value")
      .sort("sum(value)")
      .rdd.collect()
    println("India exported most in year 2018 was " +ans1(ans1.size-1))
    println()

/*
 2.Which commodity forms a major chunk of trade?
 */
    val ans2 = dataExport.groupBy("Commodity")
      .sum("value")
      .sort("sum(value)")
      .collect()

    println("Commodity which forms major chunk were "+ans2(ans2.size-1)+" and "+ans2(ans2.size-2))
    println()

    /*
    3. How has the trade between India and any given country grown over time?
     */
    val country = "U S A"
    println("the trade between India and U S A country grown over time has ")
    val ans3 = dataExport.filter(s"country = '$country'")
      .groupBy("year")
      .sum("value")
      .sort("year")
      .show()

  }
}
