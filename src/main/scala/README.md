Context
India is one of the fastest developing nations of the world and trade between nations is the major component of any developing nation. This dataset includes the trade data for India for commodities.

Content
The dataset consists of trade values for export and import of commodities in million US$. The dataset is tidy and each row consists of a single observation.

A few questions to be answered using this dataset are: 
1. What did India export the most in any given year? 
2. Which commodity forms a major chunk of trade? 
3. How has the trade between India and any given country grown over time?
4. For any particular year and country find whether India was net importer or exporter
5. For a year find if India was a net importer or exporter
6. For a given commodity find if India has been a net importer or exporter
